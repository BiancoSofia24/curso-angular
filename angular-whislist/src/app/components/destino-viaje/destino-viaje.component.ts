import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';

@Component({
  selector: 'destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje; 
  @HostBinding ('attr.class') cssClass = "col-md-4";
  // Creamos una propiedad clicked que emite el evento. Clicked es una propiedad de salida
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  // Dar a conocer al componente padre que destino fue cliqueado
  ir() {
    this.clicked.emit(this.destino); 
    return false;
  }

}
