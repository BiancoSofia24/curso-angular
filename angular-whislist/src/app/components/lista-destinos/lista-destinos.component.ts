import { Component, OnInit } from '@angular/core';
// Importar el objeto
import { DestinoViaje } from './../../models/destino-viaje.model';

@Component({
  selector: 'lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];

  constructor() {
    this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardarDestino(nombre: String, url: String): boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    // Verificamos el array
    console.log(`array: ${this.destinos}`);
    return false;
  }

  // Marcamos una como fav y desmarcamos las demás
  elegido(d: DestinoViaje) {
    this.destinos.forEach(function (x) {
      x.setSelected(false);
    });
    d.setSelected(true);
  }
}
