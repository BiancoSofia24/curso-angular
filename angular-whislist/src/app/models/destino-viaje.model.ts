export class DestinoViaje {
	// Var flag
	private selected: boolean;

	constructor(public nombre: String, public imgUrl: String) { }

	// Verficiar si está seleccionado o no
	isSelected(): boolean {
		return this.selected;
	}

	// Marcar selección
	setSelected(s: boolean) {
		this.selected = s;
	}
}
